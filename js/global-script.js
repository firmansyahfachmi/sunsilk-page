$('.hero-carousel').slick({
  autoplay: true,
  autoplaySpeed: 4000,
  dots: false,
  arrows: true,
  // fade: true
  prevArrow:
    '<button class="slick-previous"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter"><i class="icon-chevron-right"></i></button>',
  responsive: [
    {
      breakpoint: 767,
      settings: {
        arrows: false,
      },
    },
  ],
})

$('.tips-carousel').slick({
  dots: false,
  arrows: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  centerMode: true,
  prevArrow:
    '<button class="slick-previous-2 btn-purple"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter-2 btn-purple"><i class="icon-chevron-right"></i></button>',
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        variableWidth: false,
        arrows: false,
      },
    },
  ],
})
$('.product-sunsilk-carousel').slick({
  dots: false,
  arrows: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  prevArrow:
    '<button class="slick-previous-2 btn-purple"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter-2 btn-purple"><i class="icon-chevron-right"></i></button>',
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        variableWidth: false,
        arrows: false,
      },
    },
  ],
})
$('.instagram-carousel').slick({
  dots: false,
  arrows: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  centerMode: true,
  prevArrow:
    '<button class="slick-previous-2 btn-purple"><i class="icon-chevron-left"></i></button>',
  nextArrow:
    '<button class="slick-nexter-2 btn-purple"><i class="icon-chevron-right"></i></button>',
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        variableWidth: false,
        arrows: false
      },
    },
  ],
})
